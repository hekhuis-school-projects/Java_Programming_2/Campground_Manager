Campground Manager
Language: Java
Authors: Kyle Hekhuis (https://gitlab.com/hekhuisk)
Class: CIS 163 - Java Programming 2
Semester / Year: Winter 2015

This program creates a campground manager that allows you to
check-in and check-out both tents and RVs. An estimate of cost is
given upon check-in with final cost given on check-out. The current
status of the campground can be found for a specific date. Information
can be sorted by name, check-in date, days staying, or site number.
Saving and loading the state of the campground is also allowed.

Usage:
Run the main method in 'GUICampingReg' to run the program. To
check-in someone, use the options under "Checking In". To check
someone out, select them and then select "Check-Out" under
"Checking Out". Various sorting options can be found under "Sort".
To check the status for a given date, click "Campground Status" under
"File" and enter a date. Save and load features can be found under "File".