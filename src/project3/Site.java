package project3;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

/**
 * An abstract class that creates a Site object containing the information
 * of name of reserver, check-in, days staying, and site number. Includes
 * getters for all variables. Requires two abstract classes to get unique
 * information from subclasses. 
 * 
 * @version 1.0
 * 
 * @author Kyle Hekhuis
 */
public abstract class Site implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	/** The name of the person who is occupying the Site */
	protected String nameReserving;
	
	/** The date the Site was checked-in (occupied) */
	protected GregorianCalendar checkIn;
	
	/** The estimated number of days the person is reserving */
	protected int daysStaying;
	
	/** The Site number */
	protected int siteNumber;
	
	/**
	 * Creates an instance of Site with the passed parameters.
	 * 
	 * @param nameReserving the name of the person who is occupying the Site
	 * @param checkIn the date the tent was checked-in (occupied)
	 * @param daysStaying the estimated number of days the person is reserving
	 * @param siteNumber the site number
	 */
	public Site(String nameReserving, GregorianCalendar checkIn, int daysStaying, int siteNumber) {
		this.nameReserving = nameReserving;
		this.checkIn = checkIn;
		this.daysStaying = daysStaying;
		this.siteNumber = siteNumber;
	}
	
	/**
	 * Returns the name of the person who is occupying the Site.
	 * 
	 * @return name of reserver
	 */
	public String getNameReserving() {
		return nameReserving;
	}
	
	/**
	 * Returns the date the Site was checked-in (occupied)
	 * as a GregorianCalendar.
	 * 
	 * @return check-in (GregorianCalendar)
	 */
	public GregorianCalendar getCheckIn() {
		return checkIn;
	}
	
	/**
	 * Returns the date the Site was checked-in (occupied)
	 * as a String.
	 * 
	 * @return check-in (String)
	 */
	public String getCheckInString() {
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		return df.format(checkIn.getTime());
	}
	
	/**
	 * Returns the estimated number of days the person is reserving.
	 * 
	 * @return days staying
	 */
	public int getDaysStaying() {
		return daysStaying;
	}
	
	/**
	 * Returns the site number.
	 * 
	 * @return site number
	 */
	public int getSiteNumber() {
		return siteNumber;
	}
	
	/**
	 * Returns the information that is unique to the
	 * subclass as an int.
	 * 
	 * @return subclass unique info (int)
	 */
	abstract int getUniqueInfo();
	
	/**
	 * Returns the information that is unique to the
	 * subclass as a string.
	 * 
	 * @return subclass unique info (String)
	 */
	abstract String getUniqueInfoString();
}