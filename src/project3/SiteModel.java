package project3;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.Scanner;

import javax.swing.table.AbstractTableModel;

/**
 * SiteModel is a table class that stores information
 * on an ArrayList of Sites. Contains methods for making
 * the table, sorting the table, and saving the table.
 * 
 * @version 1.0
 * 
 * @author Kyle Hekhuis
 */
public class SiteModel extends AbstractTableModel {

	private static final long serialVersionUID = 1L;
	
	/** List of Sites stored in the table */
	private ArrayList<Site> sites;
	
	/** Column names for the table */
	private String[] columnNames = {"Name Reserving", "Checked in", 
									"Days Staying", "Site #", "Tent/RV info"};
	
	/**
	 * Constructs a new SiteModel.
	 */
	public SiteModel() {
		sites = new ArrayList<Site>();
	}
	
	/**
	 * Returns amount of rows the table has.
	 * 
	 * @return amount of rows the table has
	 */
	public int getRowCount() {
        return sites.size();
    }

	/**
	 * Returns amount of columns the table has.
	 * 
	 * @return amount of columns the table has
	 */
    public int getColumnCount() {
        return columnNames.length;
    }

    /**
	 * Returns the name of the column at the
	 * specified column.
	 * 
	 * @param col column to get name for
	 * 
	 * @return name of column
	 */
    public String getColumnName(int col) {
        return columnNames[col];
    }

    /**
     * Gets the value of a specific field of the Object
     * at located at the passed row and column.
     * 
     * @param row row of the object
     * @param col column of the object
     * 
     * @return object with its value at specified spot
     */
    public Object getValueAt(int row, int col) {
        Object val = null;
        switch (col) {
            case 0:
                val = sites.get(row).getNameReserving();
                break;

            case 1:
                val = sites.get(row).getCheckInString();
                break;

            case 2:
                val = sites.get(row).getDaysStaying();
                break;

            case 3:
                val = sites.get(row).getSiteNumber();
                break;

            case 4:
                val = sites.get(row).getUniqueInfoString();
                break;
        }
        return val;
    }

    /**
     * Returns the Site object located at the passed index.
     * 
     * @param index index of Site in arrayList
     * 
     * @return Site object located at index
     */
    public Site get(int index) {
        return sites.get(index);
    }

    /**
     * Returns the index of the passed Site object.
     * 
     * @param s Site object
     * 
     * @return index of passed Site object
     */
    public int indexOf(Site s) {
        return sites.indexOf(s);
    }
    
    /**
     * Add the passed Site object to the table.
     * 
     * @param s Site object to add
     */
    public void add(Site s) {
        if (s != null) {
            sites.add(s);
            fireTableRowsInserted(sites.size() - 1, sites.size() - 1);
        }
    }

    /**
     * Remove the passed Site object from the table.
     * 
     * @param s Site object to remove
     */
    public void remove(Site s) {
        sites.remove(indexOf(s));
        fireTableRowsDeleted(indexOf(s), indexOf(s));
        return;
    }

    /**
     * Clear the contents of the table.
     */
    public void clear() {
        if (sites.size() > 0) {
            int size = sites.size();
            sites.clear();
            this.fireTableRowsDeleted(0, size - 1);
        }
    }
    
    /**
     * Check if the passed site number is occupied.
     * 
     * @param siteNumber site number to check
     * 
     * @return true if site is occupied, false otherwise
     */
    public boolean siteOccupied(int siteNumber) {
    	for(int i = 0; i < sites.size(); i++) {
    		if(sites.get(i).getSiteNumber() == siteNumber) {
    			return true;
    		}
    	}
    	return false;
    }
    
    /**
     * Sorts contents of the table by name of reserver.
     */
    public void sortByName() {
    	if (sites.size() > 1) {
            Collections.sort(sites, new SiteNameComparator());
            this.fireTableRowsUpdated(0, sites.size() - 1);
        }
    }
    
    /**
     * Sorts contents of the table by day of check-in.
     */
    public void sortByCheckIn() {
    	if (sites.size() > 1) {
            Collections.sort(sites, new SiteCheckInComparator());
            this.fireTableRowsUpdated(0, sites.size() - 1);
        }
    }
    
    /**
     * Sorts contents of the table by amount of days staying.
     */
    public void sortByDaysStaying() {
    	if (sites.size() > 1) {
            Collections.sort(sites, new SiteDaysStayingComparator());
            this.fireTableRowsUpdated(0, sites.size() - 1);
        }
    }
    
    /**
     * Sorts contents of the table by site number.
     */
    public void sortBySiteNumber() {
    	if (sites.size() > 1) {
            Collections.sort(sites, new SiteSiteNumberComparator());
            this.fireTableRowsUpdated(0, sites.size() - 1);
        }
    }
    
    /**
     * Saves the table as a serialized file.
     * 
     * @param filename filename to save to
     * 
     * @throws IOException if fails to save
     */
    public void saveAsSerialized(String filename) throws IOException {
		FileOutputStream fos = new FileOutputStream(filename);
		ObjectOutputStream os = new ObjectOutputStream(fos);
		os.writeObject(sites);
		os.close();
	}
    
    /**
     * Opens a serialized file and puts its contents into the table.
     * 
     * @param file file to open
     * @throws IOException if fails to open
     * @throws ClassNotFoundException if fails to find the file
     */
    @SuppressWarnings("unchecked")
	public void openFromSerialized(File file) throws IOException, ClassNotFoundException {
		FileInputStream fis = new FileInputStream(file);
		ObjectInputStream is = new ObjectInputStream(fis);
		sites = (ArrayList<Site>) is.readObject();
		fireTableRowsInserted(sites.size() - 1, sites.size() - 1);
		is.close();
	}
    
    /**
     * Saves the table as a text file.
     * 
     * @param filename filename to save to
     * 
     * @throws IOException if fails to save
     */
    public void saveAsText(String filename) throws IOException {
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(filename)));
		out.println(sites.size());
		for (int i = 0; i < sites.size(); i++) {
			Site site = sites.get(i);
			// Distinguish what the Site is an instanceof
			if(site instanceof Tent) {
				out.println("Tent");
			} else {
				out.println("RV");
			}
			out.println(site.getNameReserving());
			out.println(site.getCheckInString());
			out.println(site.getSiteNumber());
			out.println(site.getDaysStaying());
			out.println(site.getUniqueInfo());
		}
		out.close();
	}
    
    /**
     * Opens a text file and puts its contents into the table.
     * 
     * @param file file to open
     * 
     * @throws IOException if fails to load
     */
    public void openFromText(File file) throws IOException {
		clear();
    	Scanner scanner = new Scanner(file);
		int count = Integer.parseInt(scanner.nextLine().trim());
		for (int i = 0; i < count; i++) {
			// Parse type (Tent or RV)
			String type = scanner.nextLine().trim();
			// Parse name of reserver
			String nameReserving = scanner.nextLine().trim();
			// Parse check-in date
			String date = scanner.nextLine().trim();
			// Parse site number
			int siteNumber = Integer.parseInt(scanner.nextLine().trim());
			// Parse days staying
			int daysStaying = Integer.parseInt(scanner.nextLine().trim());
			// Parse unique information (tenters / power) 
			int uniqueInfo = Integer.parseInt(scanner.nextLine().trim());
			
			// Parse the date string
			int slash1 = date.indexOf("/");
	        int slash2 = date.indexOf("/", slash1 + 1);
	        int month = Integer.parseInt(date.substring(0, slash1)) - 1;
	        int day = Integer.parseInt(date.substring(slash1 + 1, slash2));
	        int year = Integer.parseInt(date.substring(slash2 + 1));
	        
	        // Create GregorianCalendar with the parsed date
	        GregorianCalendar gc = new GregorianCalendar();
            gc.set(GregorianCalendar.MONTH, month);
            gc.set(GregorianCalendar.DATE, day);
            gc.set(GregorianCalendar.YEAR, year);

            // Find out if instance of Tent or Site
			if(type.equals("Tent")) {
				Tent t = new Tent(nameReserving, gc, daysStaying, siteNumber, uniqueInfo);
				add(t);
			} else {
				RV rv = new RV(nameReserving, gc, daysStaying, siteNumber, uniqueInfo);
				add(rv);
			}
		}
		scanner.close();
	}
}
