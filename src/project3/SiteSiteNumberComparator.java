package project3;

import java.util.Comparator;

/**
 * Comparator that compares Site objects based on site number.
 * 
 * @version 1.0
 * 
 * @author Kyle Hekhuis
 */
public class SiteSiteNumberComparator implements Comparator<Site> {

	public int compare(Site s1, Site s2) {
		int s1Site = s1.getSiteNumber();
		int s2Site = s2.getSiteNumber();
        return s1Site > s2Site ? +1 : s1Site < s2Site ? -1 : 0;
    }
}
