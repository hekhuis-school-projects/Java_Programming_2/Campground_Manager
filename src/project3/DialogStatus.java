package project3;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * Creates a JDialog that displays the status of the campground.
 * 
 * @version 1.0
 * 
 * @author Kyle Hekhuis
 */
public class DialogStatus extends JDialog implements ActionListener{
	
	private static final long serialVersionUID = 1L;
	
	private JPanel panel;
	private JButton okButton;
	
	/**
	 * Constructs a new DialogStatus with the passed 
	 * frame passed as frame to the parent class and
	 * the passed panel set as this panel.
	 * 
	 * @param frame frame of caller
	 * @param panel panel containing textarea to be displayed
	 */
	@SuppressWarnings("static-access")
	public DialogStatus(JFrame frame, JPanel panel) {
		super(frame, true);
		this.panel = panel;
		
		// Create button and center on bottom of panel
		okButton = new JButton("OK");
		okButton.setAlignmentX(panel.CENTER_ALIGNMENT);
		okButton.addActionListener(this);
		this.panel.add(okButton);
		
		setTitle("Camp Status");
		setContentPane(this.panel);
		setResizable(false);
		pack();
		setLocationRelativeTo(null);
	}
	
	/**
	 * Checks if a button was pressed and performs that button's action.
	 */
	public void actionPerformed(ActionEvent e) {
		// Closes the window
		if (e.getSource() == okButton) {
			dispose();
		}
	}
}
