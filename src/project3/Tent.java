package project3;

import java.util.GregorianCalendar;

/**
 * Creates a tent object that extends Site with the added field
 * of number of tenters.
 * 
 * @version 1.0
 * 
 * @author Kyle Hekhuis
 */
public class Tent extends Site {

	private static final long serialVersionUID = 1L;
	
	/** Represents the number of tenters on this site */
	private int numOfTenters;
	
	/**
	 * Creates an instance of Tent with the passed parameters.
	 * 
	 * @param nameReserving the name of the person who is occupying the Site
	 * @param checkIn the date the tent was checked-in (occupied)
	 * @param daysStaying the estimated number of days the person is reserving
	 * @param siteNumber the site number
	 * @param numOfTenters the number of tenters on this site
	 */
	public Tent(String nameReserving, GregorianCalendar checkIn, int daysStaying, int siteNumber, int numOfTenters) {
		super(nameReserving, checkIn, daysStaying, siteNumber);
		this.numOfTenters = numOfTenters;
	}
	
	/**
	 * Returns the information that is unique to the Tent
	 * object (number of tenters) as an int.
	 * 
	 * @return number of tenters (int)
	 */
	public int getUniqueInfo() {
		return numOfTenters;
	}
	
	/**
	 * Returns the information that is unique to the Tent
	 * object (number of tenters) as a string.
	 * 
	 * @return number of tenters (String)
	 */
	public String getUniqueInfoString() {
		return numOfTenters + " Tenter(s)";
	}
}
