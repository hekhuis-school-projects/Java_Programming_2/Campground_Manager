package project3;

import java.util.Comparator;

/**
 * Comparator that compares Site objects based on check-in.
 * 
 * @version 1.0
 * 
 * @author Kyle Hekhuis
 */
public class SiteCheckInComparator implements Comparator<Site> {

	public int compare(Site s1, Site s2) {
        return s1.getCheckIn().compareTo(s2.getCheckIn());
    }
}