package project3;

import java.util.Comparator;

/**
 * Comparator that compares Site objects based on name of reserver.
 * 
 * @version 1.0
 * 
 * @author Kyle Hekhuis
 */
public class SiteNameComparator implements Comparator<Site> {

	public int compare(Site s1, Site s2) {
        return s1.getNameReserving().compareTo(s2.getNameReserving());
    }
}
