package project3;

import java.util.GregorianCalendar;

/**
 * Creates a RV object that extends Site with the added field
 * of power.
 * 
 * @version 1.0
 * 
 * @author Kyle Hekhuis
 */
public class RV extends Site {

	private static final long serialVersionUID = 1L;
	
	/** Represents the power supplied to the site */
	private int power; // 30, 40, 50 amps of service.
	
	/**
	 * Creates an instance of RV with the passed parameters.
	 * 
	 * @param nameReserving the name of the person who is occupying the Site
	 * @param checkIn the date the tent was checked-in (occupied)
	 * @param daysStaying the estimated number of days the person is reserving
	 * @param siteNumber the site number
	 * @param power the power supplied to the site
	 */
	public RV(String nameReserving, GregorianCalendar checkIn, int daysStaying, int siteNumber, int power) {
		super(nameReserving, checkIn, daysStaying, siteNumber);
		this.power = power;
	}
	
	/**
	 * Returns the information that is unique to the RV
	 * object (power) as an int.
	 * 
	 * @return power supplied to site (int)
	 */
	public int getUniqueInfo() {
		return power;
	}
	
	/**
	 * Returns the information that is unique to the RV
	 * object (power) as a string.
	 * 
	 * @return power supplied to site (String)
	 */
	public String getUniqueInfoString() {
		return power + " Amps needed";
	}
}
