package project3;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.util.GregorianCalendar;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.text.MaskFormatter;

/**
 * Creates a JDialog that gets the required information from the user
 * that is needed to create an instance of Tent and then passes that information
 * through its getter methods.
 * 
 * @version 1.0
 * 
 * @author Kyle Hekhuis
 */
public class DialogCheckInTent extends JDialog implements ActionListener {

	private static final long serialVersionUID = 1L;
	
	/** Max number of sites on the campground */
	private static final int MAX_CAMP_SITES = 5;
	
	private JPanel panel;
	
	/** Textfield for "Name of Reserver" */
	private JTextField nameTxt;
	/** Textfield for "Days Planned to Stay" */
	private JTextField stayingTxt;
	/** Textfield for "Occupied on Date" */
	private JFormattedTextField occupyedOnTxt;
	/** Textfield for "Requested Site Number" */
	private JTextField siteNumberTxt;
	/** Textfield for "Number of Tenters" */
	private JTextField tentersTxt;
	
	private JButton okButton;
	private JButton cancelButton;
	
	private boolean ok;
	private boolean cancel;
	
	private MaskFormatter date;
	
	/**
	 * Constructs a new DialogCheckInTent with the passed 
	 * frame passed as frame to the parent class.
	 * 
	 * @param frame frame of caller
	 */
	public DialogCheckInTent(JFrame frame) {
		super(frame, true);
		
		// Create main panel and add subpanels
		panel = new JPanel();
		panel.add(setupTextPanel());
		panel.add(setupButtonPanel());
		
		setTitle("Reserve a Tent Site");
		setSize(350, 180);
		setContentPane(panel);
		setResizable(false);
		setLocationRelativeTo(null);
	}
	
	/**
	 * Creates a JPanel with the labels and text fields
	 * for the dialog box.
	 * 
	 * @return JPanel containing text fields for dialog box
	 */
	private JPanel setupTextPanel() {
		JPanel p1 = new JPanel();
		JPanel p2 = new JPanel();
		JPanel p3 = new JPanel();
		
		GridLayout gl = new GridLayout(5, 1);
		
		// Set MaskFormatter for a date input
		try {
			date = new MaskFormatter("##/##/####");
			date.setPlaceholderCharacter('-');
		} catch (ParseException e) {}
		
		// Setup text labels
		JLabel nameLbl = new JLabel("Name of Reserver:");
		JLabel siteLbl = new JLabel("Requested Site Number:");
		JLabel occupyedLbl = new JLabel("Occupied on Date:");
		JLabel stayingLbl = new JLabel("Days Planned to Stay:");
		JLabel tentersLbl = new JLabel("Number of Tenters:");
		
		// Add labels to panel p2
		p2.setLayout(gl);
		p2.add(nameLbl);
		p2.add(siteLbl);
		p2.add(occupyedLbl);
		p2.add(stayingLbl);
		p2.add(tentersLbl);
		
		// Setup textfields
		nameTxt = new JTextField(14);
		nameTxt.setHorizontalAlignment(JTextField.CENTER);
		siteNumberTxt = new JTextField();
		siteNumberTxt.setHorizontalAlignment(JTextField.CENTER);
		siteNumberTxt.setToolTipText("Must be between 1 and " + MAX_CAMP_SITES);
		occupyedOnTxt = new JFormattedTextField(date);
		occupyedOnTxt.setHorizontalAlignment(JTextField.CENTER);
		stayingTxt = new JTextField();
		stayingTxt.setHorizontalAlignment(JTextField.CENTER);
		tentersTxt = new JTextField();
		tentersTxt.setHorizontalAlignment(JTextField.CENTER);
		
		// Add textfields to panel p3
		p3.setLayout(gl);
		p3.add(nameTxt);
		p3.add(siteNumberTxt);
		p3.add(occupyedOnTxt);
		p3.add(stayingTxt);
		p3.add(tentersTxt);
		
		// Add panels p2 and p3 to panel p1
		p1.setLayout(new GridLayout(1, 2));
		p1.add(p2);
		p1.add(p3);
		
		return p1;
	}
	
	/**
	 * Creates a JPanel with the buttons for the dialog box
	 * 
	 * @return JPanel containing buttons for dialog box
	 */
	private JPanel setupButtonPanel() {
		JPanel p = new JPanel();
		
		// Setup buttons
		okButton = new JButton("OK");
		cancelButton = new JButton("Cancel");
		
		// Add actionListeners to buttons
		okButton.addActionListener(this);
		cancelButton.addActionListener(this);
		
		// Add buttons to panel p
		p.add(okButton);
		p.add(cancelButton);
		
		return p;
	}
	
	/**
	 * Checks if a button was pressed and performs that button's action.
	 */
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == okButton) {
			ok = true;
			cancel = false;
			setVisible(false);
		}
		if (e.getSource() == cancelButton) {
			ok = false;
			cancel = true;
			setVisible(false);
		}
	}
	
	/**
	 * Returns the value of ok.
	 * 
	 * @return value of ok
	 */
	public boolean isOK() {
		return ok;
	}

	/**
	 * Returns the value of cancel.
	 * 
	 * @return value of cancel
	 */
	public boolean isCancel() {
		return cancel;
	}
	
	/**
	 * Returns the name of reserver inputted in the dialog box.
	 * 
	 * @return name of reserver inputted in the dialog box
	 * 
	 * @throws IllegalArgumentException if the parameter value is invalid
	 */
	public String getName() {
		String name = nameTxt.getText();
		// Error check for null string
		if(name.length() == 0) {
			throw new IllegalArgumentException("Invalid value for parameter \"Name of Reserver\".");
		}
		// Error check for digits
		for(int i = 0; i < name.length(); i++) {
			char temp = name.charAt(i);
			if(Character.isDigit(temp)) {
				throw new IllegalArgumentException("Invalid value for parameter \"Name of Reserver\".");
			}
		}
		
		return name;
	}
	
	/**
	 * Returns the amount of days staying inputted in the dialog box.
	 * 
	 * @return amount of days staying inputted in dialog box
	 * 
	 * @throws IllegalArgumentException if the parameter value is invalid
	 */
	public int getDaysStaying() {
		int days;
		// Error check for non-digits
		try{
			days = Integer.parseInt(stayingTxt.getText());
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException("Invalid value for parameter \"Days Planned to Stay\".");
		}
		// Error check for negative numbers
		if(days < 1) {
			throw new IllegalArgumentException("Invalid value for parameter \"Days Planned to Stay\".");
		}
		
		return days;
	}
	
	/**
	 * Returns the occupied date (check-in date) inputted in the dialog box.
	 * 
	 * @return date inputted in dialog box
	 * 
	 * @throws IllegalArgumentException if the parameter value is invalid
	 */
	public GregorianCalendar getOccupyedOn() {
		GregorianCalendar gc;
		
		// Split date into string to get month, day, and year for error checking
		String temp = occupyedOnTxt.getText();
		int slash1 = temp.indexOf("/");
        int slash2 = temp.indexOf("/", slash1 + 1);
        int month = Integer.parseInt(temp.substring(0, slash1)) - 1;
        int day = Integer.parseInt(temp.substring(slash1 + 1, slash2));
        int year = Integer.parseInt(temp.substring(slash2 + 1));
		
		try{
			gc = new GregorianCalendar();
			gc.setLenient(false);
            gc.set(GregorianCalendar.MONTH, month);
            gc.set(GregorianCalendar.DATE, day);
            gc.set(GregorianCalendar.YEAR, year);
            // Error check for real date or not
	        gc.getTime();
		} catch (Exception ex) {
			throw new IllegalArgumentException("Invalid value for parameter \"Occupied on Date\".");
	    }
        
		return gc;
	}
	
	/**
	 * Returns the site number inputted in the dialog box.
	 * 
	 * @return site number inputted in textfield
	 * 
	 * @throws IllegalArgumentException if the parameter value is invalid
	 */
	public int getSiteNumber() {
		int site;
		// Error check for non-digits
		try{
			site = Integer.parseInt(siteNumberTxt.getText());
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException("Invalid value for parameter \"Requested Site Number\".");
		}
		// Error check for negative numbers or numbers greater than MAX_CAMP_SITES
		if(site < 1 || site > MAX_CAMP_SITES) {
			throw new IllegalArgumentException("Invalid value for parameter \"Requested Site Number\".");
		}
		
		return site;
	}
	
	/**
	 * Returns the number of tenters inputted in the dialog box.
	 * 
	 * @return tenters amount of tenters inputted in textfield
	 * 
	 * @throws IllegalArgumentException if the parameter value is invalid
	 */
	public int getTenters() {
		int tenters;
		// Error check for non-digits
		try{
			tenters = Integer.parseInt(tentersTxt.getText());
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException("Invalid value for parameter \"Number of Tenters\".");
		}
		// Error check for negative numbers
		if(tenters < 1) {
			throw new IllegalArgumentException("Invalid value for parameter \"Number of Tenters\".");
		}
		
		return tenters;
	}
	
	/**
	 * Clears the text fields in the dialog box.
	 */
	public void clear() {
		nameTxt.setText(null);
		stayingTxt.setText(null);
		occupyedOnTxt.setText(null);
		siteNumberTxt.setText(null);
		tentersTxt.setText(null);
	}
}
