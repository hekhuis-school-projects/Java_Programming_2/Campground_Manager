package project3;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.util.GregorianCalendar;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.text.MaskFormatter;

/**
 * Creates a JDialog that gets a date from the user.
 * 
 * @version 1.0
 * 
 * @author Kyle Hekhuis
 */
public class DialogGetDate extends JDialog implements ActionListener{

	private static final long serialVersionUID = 1L;
	
	private JPanel panel;
	
	/** Textfield for the passed date */
	private JFormattedTextField dateTxt;
	private JLabel dateLbl;
	
	private JButton okButton;
	private JButton cancelButton;
	
	/** Name of the panel and dateLbl */
	private String title;
	
	private boolean ok;
	private boolean cancel;
	
	private MaskFormatter date;
	
	/**
	 * Constructs a new DialogGetDate with the passed 
	 * frame passed as frame to the parent class.
	 * 
	 * @param frame frame of caller
	 * @param title name of the panel and dateLbl
	 */
	public DialogGetDate(JFrame frame, String title) {
		super(frame, true);
		this.title = title;
		
		// Create main panel and add subpanels
		panel = new JPanel();
		panel.add(setupTextPanel());
		panel.add(setupButtonPanel());
		
		setTitle(this.title);
		setContentPane(panel);
		setResizable(false);
		pack();
		setLocationRelativeTo(null);
	}

	/**
	 * Creates a JPanel with the labels and text fields
	 * for the dialog box.
	 * 
	 * @return JPanel containing text fields for dialog box
	 */
	private JPanel setupTextPanel() {
		JPanel p = new JPanel();
		
		// Set MaskFormatter for a date input
		try {
			date = new MaskFormatter("##/##/####");
			date.setPlaceholderCharacter('-');
		} catch (ParseException e) {}
		
		// Setup label and textfield
		dateLbl = new JLabel(title + ":");
		dateTxt = new JFormattedTextField(date);
		dateTxt.setHorizontalAlignment(JTextField.CENTER);
		
		// Add dateLbl and dateTxt to panel p
		p.setLayout(new GridLayout(1, 2, 4, 4));
		p.add(dateLbl);
		p.add(dateTxt);
		
		return p;
	}
	
	/**
	 * Creates a JPanel with the buttons for the dialog box
	 * 
	 * @return JPanel containing buttons for dialog box
	 */
	private JPanel setupButtonPanel() {
		JPanel p = new JPanel();
		
		// Setup buttons
		okButton = new JButton("OK");
		cancelButton = new JButton("Cancel");
		
		// Add actionListeners to buttons
		okButton.addActionListener(this);
		cancelButton.addActionListener(this);
		
		// Add buttons to panel p
		p.add(okButton);
		p.add(cancelButton);
		
		return p;
	}
	
	/**
	 * Checks if a button was pressed and performs that button's action.
	 */
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == okButton) {
			ok = true;
			cancel = false;
			setVisible(false);
		}
		if (e.getSource() == cancelButton) {
			ok = false;
			cancel = true;
			setVisible(false);
		}
	}
	
	/**
	 * Returns the value of ok.
	 * 
	 * @return value of ok
	 */
	public boolean isOK() {
		return ok;
	}

	/**
	 * Returns the value of cancel.
	 * 
	 * @return value of cancel
	 */
	public boolean isCancel() {
		return cancel;
	}
	
	/**
	 * Returns the date inputted in the dialog box.
	 * 
	 * @return date inputted in dialog box
	 * 
	 * @throws IllegalArgumentException if the parameter value is invalid
	 */
	public GregorianCalendar getDate() {
		GregorianCalendar gc;
		
		// Split date into string to get month, day, and year for error checking
		String temp = dateTxt.getText();
		int slash1 = temp.indexOf("/");
        int slash2 = temp.indexOf("/", slash1 + 1);
        int month = Integer.parseInt(temp.substring(0, slash1)) - 1;
        int day = Integer.parseInt(temp.substring(slash1 + 1, slash2));
        int year = Integer.parseInt(temp.substring(slash2 + 1));
		
		try{
			gc = new GregorianCalendar();
			gc.setLenient(false);
            gc.set(GregorianCalendar.MONTH, month);
            gc.set(GregorianCalendar.DATE, day);
            gc.set(GregorianCalendar.YEAR, year);
            // Error check for real date or not
	        gc.getTime();
		} catch (Exception ex) {
			throw new IllegalArgumentException("Invalid value for parameter \"" + title + "\".");
	    }
        
		return gc;
	}
	
	/**
	 * Clears the text fields in the dialog box.
	 */
	public void clear() {
		dateTxt.setText(null);
	}
}
