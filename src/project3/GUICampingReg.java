package project3;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

import javax.swing.BoxLayout;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * A class for creating a camp reservation management program. Allows the user:
 * to check-in tents and RVs at a campground as well as check them out; to 
 * open/save the checked-in occupants list into serial and text files; to
 * get the status of occupancies on the campground; to sort the checked-in
 * occupants in the display based on name of the reserver, the check-in date,
 * amount of days staying, and site number they are staying at.
 * 
 * @version 1.0
 * 
 * @author Kyle Hekhuis
 */
public class GUICampingReg extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;
	
	/** Max number of sites on the campground */
	private static final int MAX_CAMP_SITES = 5;
	
	/** Lock to stop user filling the campground beyond the MAX_CAMP_SITES */
	private static boolean globalLock = false;
	
	// Initialize compents for the GUI (frame, menu, table)
	private JFrame frame;
	private JMenuBar menuBar;
	private JMenuItem openSerialMenuItem, 
					  saveSerialMenuItem, 
					  openTextMenuItem,
					  saveTextMenuItem,
					  statusMenuItem,
					  exitMenuItem,
					  cInTentMenuItem,
					  cInRVMenuItem,
					  cOutMenuItem,
					  sortNameMenuItem,
					  sortCheckInMenuItem,
					  sortDaysStayingMenuItem,
					  sortSiteNumberMenuItem;
	private JTable table;
	private JScrollPane scrollPane;
	
	/** SiteModel that holds all data */
	private SiteModel model;
	
	/** Dialog for checking in a tent */
	private DialogCheckInTent dialogTent;
	
	/** Dialog for checking in a RV */
	private DialogCheckInRv dialogRV;
	
	/** Dialog for getting check-out date */
	private DialogGetDate dialogCOut;
	
	/** Dialog for getting date for status check */
	private DialogGetDate dialogStatus;
	
	/** FileChooser for opening and saving files */
	private JFileChooser fc = new JFileChooser();
	
	/**
	 * Creates a new GUICampingReg with dialogs.
	 */
	public GUICampingReg() {
		setupFrame();
		addActionListeners();
		
		// Set tableModel to the SiteModel
		model = new SiteModel();
		table.setModel(model);
		
		// Create dialogs
		dialogTent = new DialogCheckInTent(this);
		dialogRV = new DialogCheckInRv(this);
		dialogCOut = new DialogGetDate(this, "Check-Out Date");
		dialogStatus = new DialogGetDate(this, "Campground Status");
	}
	
	/**
	 * Add actionListeners to all buttons.
	 */
	private void addActionListeners() {
		openSerialMenuItem.addActionListener(this);
		saveSerialMenuItem.addActionListener(this);
		openTextMenuItem.addActionListener(this);
		saveTextMenuItem.addActionListener(this);
		statusMenuItem.addActionListener(this);
		exitMenuItem.addActionListener(this);
		cInTentMenuItem.addActionListener(this);
		cInRVMenuItem.addActionListener(this);
		cOutMenuItem.addActionListener(this);
		sortNameMenuItem.addActionListener(this);
		sortCheckInMenuItem.addActionListener(this);
		sortDaysStayingMenuItem.addActionListener(this);
		sortSiteNumberMenuItem.addActionListener(this);
	}
	
	/**
	 * Setup the menu for the frame.
	 */
	private void setupMenu() {
		menuBar = new JMenuBar();
		
		// Setup File menu
		JMenu fileMenu = new JMenu("File");
		openSerialMenuItem = new JMenuItem("Open Serial");
		saveSerialMenuItem = new JMenuItem("Save Serial");
		openTextMenuItem = new JMenuItem("Open Text");
		saveTextMenuItem = new JMenuItem("Save Text");
		statusMenuItem = new JMenuItem("Campground Status");
		exitMenuItem = new JMenuItem("Exit");
		fileMenu.add(openTextMenuItem);
		fileMenu.add(saveTextMenuItem);
		fileMenu.addSeparator();
		fileMenu.add(openSerialMenuItem);
		fileMenu.add(saveSerialMenuItem);
		fileMenu.addSeparator();
		fileMenu.add(statusMenuItem);
		fileMenu.addSeparator();
		fileMenu.add(exitMenuItem);
		
		// Setup Sort menu
		JMenu sortMenu = new JMenu("Sort");
		sortNameMenuItem = new JMenuItem("Sort Name");
		sortCheckInMenuItem = new JMenuItem("Sort Check-In");
		sortDaysStayingMenuItem = new JMenuItem("Sort Days Staying");
		sortSiteNumberMenuItem = new JMenuItem("Sort Site Number");
		sortMenu.add(sortNameMenuItem);
		sortMenu.add(sortCheckInMenuItem);
		sortMenu.add(sortDaysStayingMenuItem);
		sortMenu.add(sortSiteNumberMenuItem);
		
		// Setup Check-In menu
		JMenu cInMenu = new JMenu("Checking In");
		cInTentMenuItem = new JMenuItem("Check-In Tent");
		cInRVMenuItem = new JMenuItem("Check-In RV");
		cInMenu.add(cInTentMenuItem);
		cInMenu.add(cInRVMenuItem);
		
		// Setup Check-Out menu
		JMenu cOutMenu = new JMenu("Checking Out");
		cOutMenuItem = new JMenuItem("Check-Out");
		cOutMenu.add(cOutMenuItem);
		cOutMenuItem.setEnabled(false);
		
		// Add menus to menu bar
		menuBar.add(fileMenu);
		menuBar.add(sortMenu);
		menuBar.add(cInMenu);
		menuBar.add(cOutMenu);
	}
	
	/**
	 * Setup the main frame of the GUI by adding
	 * MenuBar and table.
	 */
	private void setupFrame() {
		frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(700,200);
		
		setupMenu();
		frame.setJMenuBar(menuBar);
		
		setupTable();
		frame.add(scrollPane);
		
		frame.setLocationRelativeTo(null);
		frame.setResizable(true);
		frame.setVisible(true);
	}
	
	/**
	 * Setup the table to be added to the frame.
	 */
	private void setupTable() {
		table = new JTable();
		scrollPane = new JScrollPane(table);
        
        scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        table.setToolTipText("");
        table.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        table.getTableHeader().setReorderingAllowed(false);
        scrollPane.setViewportView(table);
	}
	
	/**
	 * Checks if a button was pressed and performs that button's action.
	 */
	public void actionPerformed(ActionEvent e) {
		// Opens text file
		if(e.getSource() == openTextMenuItem) {
			try {
				openText();
			} catch (IllegalArgumentException ex) {
				JOptionPane.showMessageDialog(frame, ex);
			}
			hasSites();
		}
		// Saves text file
		if(e.getSource() == saveTextMenuItem) {
			try {
				saveText();
			} catch (IllegalArgumentException ex) {
				JOptionPane.showMessageDialog(frame, ex);
			} catch (NullPointerException ex) {}
		}
		// Opens serial file
		if(e.getSource() == openSerialMenuItem) {
			try {
				openSerial();
			} catch (IllegalArgumentException ex) {
				JOptionPane.showMessageDialog(frame, ex);
			}
			hasSites();
		}
		// Saves serial file
		if(e.getSource() == saveSerialMenuItem) {
			try {
				saveSerial();
			} catch (IllegalArgumentException ex) {
				JOptionPane.showMessageDialog(frame, ex);
			} catch (NullPointerException ex) {}
		}
		// Shows status of campground
		if(e.getSource() == statusMenuItem) {
			status();
		}
		// Exits program
		if(e.getSource() == exitMenuItem) {
			System.exit(0);
		}
		// Sorts table by name
		if(e.getSource() == sortNameMenuItem) {
			model.sortByName();
		}
		// Sorts table by check-in date
		if(e.getSource() == sortCheckInMenuItem) {
			model.sortByCheckIn();
		}
		// Sorts table by days staying
		if(e.getSource() == sortDaysStayingMenuItem) {
			model.sortByDaysStaying();
		}
		// Sorts table by site number
		if(e.getSource() == sortSiteNumberMenuItem) {
			model.sortBySiteNumber();
		}
		// Check-in tent
		if(e.getSource() == cInTentMenuItem) {
			addTent();
		}
		// Check-in RV
		if(e.getSource() == cInRVMenuItem) {
			addRV(e);
		}
		// Check-out site
		if(e.getSource() == cOutMenuItem) {
			checkOut();
		}
	}

	/**
	 * Opens a serial file to be loaded into the table.
	 * 
	 * @throws IllegalArgumentException if failed to open file
	 */
	private void openSerial() {
		fc.setFileFilter(new FileNameExtensionFilter("Serial files", "ser"));
		fc.setCurrentDirectory(new File("/home/me/Documents"));
		int temp = fc.showOpenDialog(null);
		if(temp == JFileChooser.APPROVE_OPTION) {
			try {
				model.openFromSerialized(fc.getSelectedFile());
			} catch (ClassNotFoundException e1) {
				throw new IllegalArgumentException("Failed to open file.");
			} catch (IOException e1) {
				throw new IllegalArgumentException("Failed to open file.");
			}
		}
		fc.setSelectedFile(new File(""));
	}
	
	/**
	 * Saves a serial file of the contents of the table.
	 * 
	 * @throws IllegalArgumentException if failed to save file
	 */
	private void saveSerial() {
		fc.setFileFilter(new FileNameExtensionFilter("Serial files", "ser"));
		fc.setCurrentDirectory(new File("/home/me/Documents"));
		int temp = fc.showSaveDialog(null);
		String check = fc.getSelectedFile().getName();
		
		if(temp == JFileChooser.APPROVE_OPTION) {
			try {
				if(check.substring(check.length() - 4).equalsIgnoreCase(".ser")) {
					model.saveAsSerialized(fc.getSelectedFile() + "");
				} else {
					model.saveAsSerialized(fc.getSelectedFile() + ".ser");
				}
			} catch (IOException e1) {
				throw new IllegalArgumentException("Failed to save file.");
			}
		}		
		fc.setSelectedFile(new File(""));
	}
	
	/**
	 * Opens a text file to be loaded into the table.
	 * 
	 * @throws IllegalArgumentException if failed to open file
	 */
	private void openText() {
		fc.setFileFilter(new FileNameExtensionFilter("Text files", "txt"));
		fc.setCurrentDirectory(new File("/home/me/Documents"));
		int temp = fc.showOpenDialog(null);
		if(temp == JFileChooser.APPROVE_OPTION) {
			try {
				model.openFromText(fc.getSelectedFile());
			} catch (IOException ex) {
				throw new IllegalArgumentException("Failed to open file.");
			}
		}
		fc.setSelectedFile(new File(""));
	}
	
	/**
	 * Saves a text file of the contents of the table.
	 * 
	 * @throws IllegalArgumentException if failed to save file
	 */
	private void saveText() {
		fc.setFileFilter(new FileNameExtensionFilter("Text files", "txt"));
		fc.setCurrentDirectory(new File("/home/me/Documents"));
		int temp = fc.showSaveDialog(null);
		String check = fc.getSelectedFile().getName();
		if(temp == JFileChooser.APPROVE_OPTION) {
			try {
				if(check.substring(check.length() - 4).equalsIgnoreCase(".txt")) {
					model.saveAsText(fc.getSelectedFile() + "");
				} else {
					model.saveAsText(fc.getSelectedFile() + ".txt");
				}
			} catch (IOException ex) {
				throw new IllegalArgumentException("Failed to save file.");
			}
		}
		fc.setSelectedFile(new File(""));
	}
	
	/**
	 * Adds a tent to the table based off passed information from DialogCheckInTent.
	 */
	private void addTent() {
		// Check if campground full
		if(globalLock) {
			JOptionPane.showMessageDialog(frame, "All sites are occupied!", "Campground Full!", JOptionPane.WARNING_MESSAGE);
			return;
        }
		
		// Clear and display check-in dialog for tent
		dialogTent.clear();
		dialogTent.setVisible(true);
		if(dialogTent.isOK()) {
			// Create new tent
			try{
				Tent t = new Tent(dialogTent.getName(),
								  dialogTent.getOccupyedOn(),
								  dialogTent.getDaysStaying(),
								  dialogTent.getSiteNumber(),
								  dialogTent.getTenters());
				checkIn(0, dialogTent.getDaysStaying(), dialogTent.getSiteNumber(), dialogTent.getTenters());
				model.add(t);
			} catch (IllegalArgumentException ex) {
				JOptionPane.showMessageDialog(frame, ex);
			}
		}
		// Check if campground has sites or is full
		hasSites();
		checkCampFull();
	}
	
	private void addRV(ActionEvent e) {
		// Check if campground full
		if(globalLock) {
			JOptionPane.showMessageDialog(frame, "All sites are occupied!", "Campground Full!", JOptionPane.WARNING_MESSAGE);
			return;
        }
		
		// Clear and display check-in dialog for RV
		dialogRV.clear();
		dialogRV.setVisible(true);
		if(dialogRV.isOK()) {
			// Create new RV
			try{
				RV rv = new RV(dialogRV.getName(),
							   dialogRV.getOccupyedOn(),
						       dialogRV.getDaysStaying(),
						       dialogRV.getSiteNumber(),
						       dialogRV.getPower());
				checkIn(1, dialogRV.getDaysStaying(), dialogRV.getSiteNumber(), 0);
				model.add(rv);
			} catch (IllegalArgumentException ex) {
				JOptionPane.showMessageDialog(frame, ex);
			}
		}
		// Check if campground has sites or is full
		hasSites();
		checkCampFull();
	}
	
	/**
	 * Checks if a site is already occupied and calculates estimate
	 * of what the stay will cost based off days staying.
	 * 
	 * @param type type of Site (0 for tent, 1 for RV)
	 * @param days days staying
	 * @param site site to check if occupied
	 * @param tenters amount of tenters (0 if checking in RV)
	 */
	private void checkIn(int type, int days, int site, int tenters) {
		// Check if site occupied
		if(model.siteOccupied(site)) {
			throw new IllegalArgumentException("That site is already occupied.");
		}
		
		// Calculate estimated cost of stay
		int cost = calculateCost(type, days, tenters);
		JOptionPane.showMessageDialog(frame, "You Owe: $" + cost + ".00", "Amount due - ESTIMATE", JOptionPane.INFORMATION_MESSAGE);
	}
	
	/**
	 * Checks out the selected site in the table and calculates final
	 * cost of stay.
	 */
	private void checkOut() {
		// Checks if site is selected
		if(table.getSelectedRow() >= 0) {
			// Confirm if want to check-out
			int temp = JOptionPane.showConfirmDialog(frame, "Confirm Check-Out", "Check-Out Confirmation", JOptionPane.OK_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE);
			if(temp == JOptionPane.CANCEL_OPTION) {
				return;
			}
			
			// Variables for day of check-in,
			// day of check-out, type of Site,
			// and amount of tenters if tent site
	    	GregorianCalendar checkInDay;
	    	GregorianCalendar checkOutDay;
	    	int type = 0;
	    	int tenters = 0;
	    	
	    	int index = table.getSelectedRow();
	    	// Changes type to 1 for RV or
	    	// sets amount of tenters if it's a tent
	    	if(model.get(index) instanceof RV) {
	    		type = 1;
	    	} else {
	    		tenters = model.get(index).getUniqueInfo();
	    	}
	    	
	    	// Clear and display check-in dialog for RV
			dialogCOut.clear();
			dialogCOut.setVisible(true);
			if(dialogCOut.isOK()) {
				checkOutDay = dialogCOut.getDate();
				checkInDay = model.get(index).getCheckIn();
				int cost = calculateCost(type, calculateDiffInDays(checkInDay, checkOutDay), tenters);
				JOptionPane.showMessageDialog(frame, "You Owe: $" + cost + ".00", "Amount due - FINAL AMOUNT", JOptionPane.INFORMATION_MESSAGE);
			}
			
	    	model.remove(model.get(index));
	        
	    	// Check if campground has sites or is full
	        hasSites();
	        checkCampFull();
		} else {
			JOptionPane.showMessageDialog(frame, "Please select a site to check-out", "", JOptionPane.INFORMATION_MESSAGE);
		}
	}
	
	/**
	 * Checks if table has sites and enables
	 * check-out feature if it does, or disables
	 * otherwise.
	 */
	private void hasSites() {
		if(model.getRowCount() > 0) {
			cOutMenuItem.setEnabled(true);
		} else {
			cOutMenuItem.setEnabled(false);
		}
	}
	
	/**
	 * Displays the status panel of the campground.
	 */
	private void status() {
		dialogStatus.clear();
		dialogStatus.setVisible(true);
		if(dialogStatus.isOK()) {
			GregorianCalendar date = dialogStatus.getDate();
			DialogStatus tempDialog = new DialogStatus(this, setupStatusPanel(date));
			tempDialog.setVisible(true);
		}
	}
	
	/**
	 * Setsup the status panel to be displayed except for buttons.
	 * 
	 * @param date date to check status for
	 * 
	 * @return JPanel containing text and labels for status
	 */
	@SuppressWarnings("static-access")
	private JPanel setupStatusPanel(GregorianCalendar date) {
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		JLabel Lbl = new JLabel("Camp Status for the Date: " + df.format(date.getTime()));
		Lbl.setAlignmentX(panel.CENTER_ALIGNMENT);
		
		JTextArea ta = new JTextArea();
		// Fill text area
		for(int i = 0; i < model.getRowCount(); i++) {
			String temp = "";
			temp += model.get(i).getNameReserving() + "\t";
			temp += "Checked in: " + model.get(i).getCheckInString() + "\t";
			temp += "Site # " + model.get(i).getSiteNumber() + "\t";
			temp += "Estimated Days: " + model.get(i).getDaysStaying() + "\t";
			temp += "Days Remaining: " + calculateDiffInDays(date, addDays(model.get(i).getCheckIn(), model.get(i).getDaysStaying() ) );
			
			ta.append(temp + "\n");
		}
		
		panel.add(Lbl);
		ta.setText(ta.getText().trim());
		ta.setEditable(false);
		panel.add(ta);
		
		return panel;
	}
	
	/**
	 * Adds passed days to a date for purpose of calculating check-out cost.
	 * 
	 * @param gc date to add to
	 * @param days amount of days to add
	 * 
	 * @return GregorianCalendar with new date
	 */
	private GregorianCalendar addDays(GregorianCalendar gc, int days) {
		GregorianCalendar gcTemp = new GregorianCalendar();
		gcTemp.setTime(gc.getTime());
		gcTemp.add(GregorianCalendar.DATE, days);
		return gcTemp;
	}
	
	/**
	 * Calculates the difference in days between two dates.
	 * 
	 * @param day1 day to subtract
	 * @param day2 day to subtract from
	 * 
	 * @return difference in days between the two passed dates
	 */
	private int calculateDiffInDays(GregorianCalendar day1, GregorianCalendar day2) {
		return (int)( (day2.getTimeInMillis() - day1.getTimeInMillis()) / (1000 * 60 * 60 * 24) );
	}
	
	/**
	 * Calculates the cost of the stay.
	 * 
	 * @param type type of site (0 for tent, 1 for RV)
	 * @param days amount of days actually stayed
	 * @param tenters amount of tenters (0 if RV)
	 * 
	 * @return cost of the stay
	 */
	private int calculateCost(int type, int days, int tenters) {
		int cost;
		if(type == 0) {
			cost = 3 * days * tenters;
		} else {
			cost = 30 * days;
		}
		return cost;
	}
	
	/**
	 * Checks if the campground is full and sets the lock to true if it is,
	 * or false otherwise.
	 */
	private void checkCampFull() {
		if(model.getRowCount() == MAX_CAMP_SITES) {
			globalLock = true;
			JOptionPane.showMessageDialog(frame, "All sites are occupied!", "Campground Full!", JOptionPane.WARNING_MESSAGE);
		} else {
			globalLock = false;
		}
	}
	
	/**
	 * Runs the GUI.
	 * 
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new GUICampingReg();
            }
        });
    }
}
