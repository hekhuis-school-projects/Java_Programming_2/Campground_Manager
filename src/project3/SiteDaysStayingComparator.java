package project3;

import java.util.Comparator;

/**
 * Comparator that compares Site objects based on days staying.
 * 
 * @version 1.0
 * 
 * @author Kyle Hekhuis
 */
public class SiteDaysStayingComparator implements Comparator<Site> {

	public int compare(Site s1, Site s2) {
		int s1Days = s1.getDaysStaying();
		int s2Days = s2.getDaysStaying();
        return s1Days > s2Days ? +1 : s1Days < s2Days ? -1 : 0;
    }
}
